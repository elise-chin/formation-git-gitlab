# Formation Git et GitHub

## Index

- [Prérequis](#wrench-prérequis)
    - [Python](#installer-python)
    - [Visual Studio Code](#installer-visual-studio-code-vscode)
    - [GitHub](#créer-un-compte-github)
    - [Git](#installer-git)
- [Tutoriel Git](#pencil2-tutoriel-git)
- [Troubleshooting](#computer-troubleshooting)

## :wrench: Prérequis

### Installer Python

[Télécharger la dernière version de Python](https://www.python.org/downloads/)

### Installer Visual Studio Code (VSCode)

Installer un environnement de développement, e.g. [Visual Studio Code (VSCode)](https://code.visualstudio.com/download).

### Créer un compte Gitlab

Gitlab est une plateforme en ligne qui permet, entre autres, d'héberger des dépôts Git.

1. Allez sur https://about.gitlab.com/ et cliquez sur "sign in", puis "Register now"
2. Remplissez le formulaire avec votre nom, prénom, email, nom d'utilisateur (qui peut être modifié plus tard), et un mot de passe
3. Vérifiez votre adresse email

Vous pouvez également vous connecter avec votre compte GitHub si vous en avez.

#### Clé SSH

Avec une clé SSH, vous pouvez vous connecter à GitHub depuis votre machine sans fournir votre nom d'utilisateur et votre personal access token à chaque visite. Vous pouvez également utiliser une clé SSH pour signer des commits. 
Nous allons donc ajouter une nouvelle clé SSH à notre compte Gitlab, pour lier notre machine locale au compte.
Veuillez suivre les étapes décrites [ici](https://docs.gitlab.com/ee/user/ssh.html)

### Installer Git

Git est le gestionnaire de versions le plus largement utilisé aujourd'hui. Il permet de conserver un historique des modifications et des versions de chaque fichier d'un projet. 

1. [Télécharger la dernière version de Git](https://git-scm.com/download/win)  
2. Accepter tous les choix par défaut
3. Dans un terminal (e.g. Windows PowerShell ou Git bash), tapez `git` pour vérifier la bonne installation
4. Dans ce même terminal, configurer votre identité. Renseigner l’email que vous avez utilisé pour créer votre compte GitHub.
    ```bash
    git config --global user.name "John Doe"
    git config --global user.email "johndoe@example.com"
    ```

## :pencil2: Tutoriel Git

- [Tutoriel Git et GitHub](https://elxse.notion.site/Git-et-Github-57337f89f6ae4ca9b6097507e88a1030)
- [Cheatsheet](https://training.github.com/downloads/fr/github-git-cheat-sheet.pdf)

## :computer: Troubleshooting

Si vous rencontrez quelconque problème ou avez des questions, n'hésitez pas à envoyer un mail à c.chin.elise@gmail.com